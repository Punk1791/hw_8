﻿namespace hw_8
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
