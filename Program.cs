﻿﻿using System;

namespace hw_8
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            //Тесты
            var te0 = new Test<Guitar>(new bool[4]{false, false, true, true});
            te0.ThisTest(new Guitar());

            var te1 = new Test<LoverDeepRiffs>(new bool[4]{false, false, true, true});
            te1.ThisTest(new LoverDeepRiffs());

            var te2 = new Test<Blackmore>(new bool[4]{false, false, true, true});
            te2.ThisTest(new Blackmore());

            var te3 = new Test<Malmsteen>(new bool[4]{true, true, true, true});
            te3.ThisTest(new Malmsteen());
            
        }
    }

    public class Test<T> where T:Guitar
    {
        public bool[] answer = new bool[4];
        public Test(bool[] ans)
        {
            answer = ans;
        }
        public void CustomEqals (object A, object B, bool Right)
        {
            Console.WriteLine($"А:{A.GetType()}, B:{B.GetType()}. Равны? {A.Equals(B)}. А должны? {A.Equals(B) == Right} ");
        }

        public void ThisTest (T dfg)
        {
            CustomEqals(dfg, dfg.MyClone(), answer[0]);
            CustomEqals(dfg, dfg.Clone(), answer[1]);

            dfg.IsFlydeRose = false;

            var tcl = (Guitar)dfg.Clone();
            CustomEqals(dfg.IsFlydeRose, dfg.MyClone().IsFlydeRose, answer[2]);
            CustomEqals(dfg.IsFlydeRose, tcl.IsFlydeRose, answer[3]);

            Console.WriteLine("____________________________________________");
        }
    }
}