using System;

namespace hw_8
{
    public class LoverDeepRiffs : Guitar
    {
        public int AdditionallyString { get; set; } = 2;

        public LoverDeepRiffs(){}

        public LoverDeepRiffs(LoverDeepRiffs dryndryn) : base(dryndryn.NumStrings, dryndryn.NumHumbucker, dryndryn.NumSingle, dryndryn.IsSteep, dryndryn.IsFlydeRose, dryndryn.Color)
        {
            NumStrings += AdditionallyString;
            IsFlydeRose = true;
            Color = "Black";
            IsSteep = false;
        }

        public LoverDeepRiffs(int _NumStrings, int _NumHumbucker, int _NumSingle, bool _IsSteep, bool _IsFlydeRose, string _Color) 
            : base(_NumStrings, _NumHumbucker, _NumSingle, _IsSteep, _IsFlydeRose, _Color)
        {
            NumStrings +=  _NumStrings;
            IsFlydeRose = true;
            Color = _Color;
            IsSteep = false;
        }

        public override void SetColor(string NewColor)
        {
            Color = NewColor;
        }

        public override bool ReturnSteep()
        {
            return IsSteep;
        }

    }
}