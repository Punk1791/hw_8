﻿using System;

namespace hw_8
{
    public class Guitar : IMyCloneable<Guitar>, ICloneable
    {
        public int NumStrings { get; set; } = 6;
        public int NumHumbucker { get; set; }
        public int NumSingle { get; set; }
        public bool IsSteep { get; set; }
        public bool IsFlydeRose { get; set; }
        public string Color { get; set; }

        public Guitar(){}

        public Guitar(int _NumStrings, int _NumHumbucker, int _NumSingle, bool _IsSteep, bool _IsFlydeRose, string _Color)
        {
            NumStrings = _NumStrings;
            NumHumbucker = _NumHumbucker;
            NumSingle = _NumSingle;
            IsSteep = _IsSteep;
            IsFlydeRose = _IsFlydeRose;
            Color = _Color;
        }

        public Guitar(Guitar drynk)
        {
            NumStrings = drynk.NumStrings;
            NumHumbucker = drynk.NumHumbucker;
            NumSingle = drynk.NumSingle;
            IsSteep = drynk.IsSteep;
            IsFlydeRose = drynk.IsFlydeRose;
            Color = drynk.Color;
        }

        public object Clone()
        {
            return MyClone();
        }

        public virtual Guitar MyClone()
        {
            return new Guitar(this);
        }

        public virtual void SetColor(string NewColor)
        {
            Color = NewColor;
        }

        public virtual bool ReturnSteep()
        {
            return IsSteep;
        }
    }
}