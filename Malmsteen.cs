using System;

namespace hw_8
{
    public class Malmsteen : Blackmore
    {
        public string ActualColor { get; set; } = "sunburst";

        public Malmsteen(){}

        public Malmsteen(Malmsteen dryndryn) : base(dryndryn.NumStrings, dryndryn.NumHumbucker, dryndryn.NumSingle, dryndryn.IsSteep, dryndryn.IsFlydeRose, dryndryn.Color)
        {
            NumHumbucker = 1;
            NumSingle = 2;
            IsFlydeRose = true;
            Color = ActualColor;
            IsSteep = true;
        }

        public Malmsteen(int _NumStrings, int _NumHumbucker, int _NumSingle, bool _IsSteep, bool _IsFlydeRose, string _Color) 
            : base(_NumStrings, _NumHumbucker, _NumSingle, _IsSteep, _IsFlydeRose, _Color)
        {
            NumHumbucker = _NumHumbucker;
            NumSingle = _NumSingle;
            IsFlydeRose = true;
            Color = _Color;
            IsSteep = true;
        }

        public override Malmsteen MyClone()
        {
            return this;
        }

    }
}