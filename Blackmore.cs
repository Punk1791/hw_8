using System;

namespace hw_8
{
    public class Blackmore : Guitar
    {
        public string ActualColor { get; set; } = "white";

        public Blackmore(){}

        public Blackmore(Blackmore dryndryn) : base(dryndryn.NumStrings, dryndryn.NumHumbucker, dryndryn.NumSingle, dryndryn.IsSteep, dryndryn.IsFlydeRose, dryndryn.Color)
        {
            NumHumbucker = 0;
            NumSingle = 2;
            IsFlydeRose = true;
            Color = ActualColor;
            IsSteep = true;
        }

        public Blackmore(int _NumStrings, int _NumHumbucker, int _NumSingle, bool _IsSteep, bool _IsFlydeRose, string _Color) 
            : base(_NumStrings, _NumHumbucker, _NumSingle, _IsSteep, _IsFlydeRose, _Color)
        {
            NumHumbucker = _NumHumbucker;
            NumSingle = _NumSingle;
            IsFlydeRose = true;
            Color = _Color;
            IsSteep = true;
        }

        public override void SetColor(string NewColor)
        {
            Color = NewColor;
        }

        public override bool ReturnSteep()
        {
            return IsSteep;
        }

    }
}